## 添加upush maven仓库
在pom.xml添加
```
<repositories>
    <repository>
        <id>upush</id>
        <url>https://maven.cmcim.com:7216/repository/upush/</url>
        <releases>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
        </releases>
    </repository>
</repositories>
```

## 依赖
在pom.xml添加
```
<dependency>
    <groupId>com.cmcim.upush</groupId>
    <artifactId>upush2-api</artifactId>
    <version>2.0.1</version>
</dependency>
```

## 配置
打开application.yml
```
upush:
  app-secret: UPush项目密钥
  host: master地址
  port: master端口
```