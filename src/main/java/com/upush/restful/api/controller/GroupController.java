/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.controller;

import com.upush.restful.api.dto.*;
import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushClient;
import com.upush.api.UPushResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private UPushClient pushClient;

    /**
     * 设置groupId群组画像
     * @param portrait
     * @return
     */
    @PostMapping("/portrait")
    public DeferredResult<UPushResponse> setPortraitForGroup(@Validated @RequestBody GroupPortraitDto portrait){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setPortraitForGroup(portrait.getGroupId(), portrait.getName(), portrait.getAvatar())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 删除群组/订阅项
     * @param portrait
     * @return
     */
    @DeleteMapping("/{groupId}")
    public DeferredResult<UPushResponse> deleteGroup(@Validated @RequestBody GroupPortraitDto portrait){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setPortraitForGroup(portrait.getGroupId(), portrait.getName(), portrait.getAvatar())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定pushToken订阅到群组
     * @param subscribe
     * @return
     */
    @PostMapping("/pushToken/subscribe")
    public DeferredResult<UPushResponse> tokenSubscribe(@Validated @RequestBody TokenSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenSubscribe(subscribe.getGroupId(), subscribe.getPushToken())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 多个pushToken订阅到群组
     * @param subscribe
     * @return
     */
    @PostMapping("/multi/pushToken/subscribe")
    public DeferredResult<UPushResponse> tokensSubscribe(@Validated @RequestBody MultiTokenSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokensSubscribe(subscribe.getGroupId(), subscribe.getPushTokens())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定pushToken订阅多个群组
     * @param subscribe
     * @return
     */
    @PostMapping("/pushToken/subscribe/multi")
    public DeferredResult<UPushResponse> tokenMultiSubscribe(@Validated @RequestBody TokenMultiSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenMultiSubscribe(subscribe.getGroupIds(), subscribe.getPushToken())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定pushToken取消订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/pushToken/unsubscribe")
    public DeferredResult<UPushResponse> tokenUnSubscribe(@Validated @RequestBody TokenSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenUnsubscribe(subscribe.getGroupId(), subscribe.getPushToken())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 多个pushToken取消订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/multi/pushToken/unsubscribe")
    public DeferredResult<UPushResponse> tokensUnSubscribe(@Validated @RequestBody MultiTokenSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokensUnsubscribe(subscribe.getGroupId(), subscribe.getPushTokens())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 设置pushToken在群groupId中的通知类型
     * @param groupId
     * @param pushToken
     * @param notifyType
     * @return
     */
    @PostMapping("/{groupId}/pushToken/{pushToken}/notification/type/{notifyType}")
    public DeferredResult<UPushResponse> setGroupNotifyTypeForToken(@PathVariable("groupId") String groupId,
                                                                    @PathVariable("pushToken") String pushToken,
                                                                    @PathVariable("notifyType") Integer notifyType){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setGroupNotifyTypeForToken(groupId, pushToken, notifyType)
                .send(response -> result.setResult(response));
        return result;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * 指定用户订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/user/subscribe")
    public DeferredResult<UPushResponse> userSubscribe(@Validated @RequestBody UserSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.userSubscribe(subscribe.getGroupId(), subscribe.getUserId())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定多个用户订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/multi/user/subscribe")
    public DeferredResult<UPushResponse> usersSubscribe(@Validated @RequestBody MultiUserSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.usersSubscribe(subscribe.getGroupId(), subscribe.getUserIds())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定用户取消订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/user/unsubscribe")
    public DeferredResult<UPushResponse> userUnSubscribe(@Validated @RequestBody UserSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.userUnsubscribe(subscribe.getGroupId(), subscribe.getUserId())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 指定多个用户取消订阅群组
     * @param subscribe
     * @return
     */
    @PostMapping("/multi/user/unsubscribe")
    public DeferredResult<UPushResponse> usersUnSubscribe(@Validated @RequestBody MultiUserSubscribe subscribe){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.usersUnsubscribe(subscribe.getGroupId(), subscribe.getUserIds())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 设置userId在群groupId中的通知类型
     * @param groupId
     * @param userId
     * @param notifyType
     * @return
     */
    @PostMapping("/{groupId}/user/{userId}/notification/type/{notifyType}")
    public DeferredResult<UPushResponse> setGroupNotifyTypeForUser(@PathVariable("groupId") String groupId,
                                                                   @PathVariable("userId") String userId,
                                                                   @PathVariable("notifyType") Integer notifyType){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setGroupNotifyTypeForUser(groupId, userId, notifyType)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * userId发送groupId群消息时，显示userId的别名
     * @param groupId
     * @param userId
     * @param alias
     * @return
     */
    @PostMapping("/{groupId}/user/{userId}/alias/{alias}")
    public DeferredResult<UPushResponse> setAliasForUserInGroup(@PathVariable("groupId") String groupId,
                                                                @PathVariable("userId") String userId,
                                                                @PathVariable("alias") String alias){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setAliasForUserInGroup(groupId, userId, alias)
                .send(response -> result.setResult(response));
        return result;
    }
}
