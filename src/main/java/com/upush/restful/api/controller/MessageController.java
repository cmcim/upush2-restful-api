/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.controller;

import com.upush.api.constant.ErrorCode;
import com.upush.restful.api.dto.*;
import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushClient;
import com.upush.api.UPushResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private UPushClient pushClient;

    /**
     * 给指定pushToken推送消息
     * @param msg
     * @return
     */
    @PostMapping("/pushToken")
    public DeferredResult<UPushResponse> pushToToken(@Validated @RequestBody UMessageToken msg){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenPush(msg.getMessage(), msg.getPushToken())
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 给多个pushToken推送消息
     * @param msg
     * @return
     */
    @PostMapping("/multi/pushToken")
    public DeferredResult<UPushResponse> pushToTokens(@Validated @RequestBody UMessageTokenMulti msg){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokensPush(msg.getMessage(), msg.getPushTokens())
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 从指定pushToken撤回消息
     * @param msg
     * @return
     */
    @PostMapping("/revoke/pushToken")
    public DeferredResult<UPushResponse> revokeFromToken(@Validated @RequestBody URevokeMessageToken msg){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenRevoke(msg.getMessage(), msg.getPushToken())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 从多个pushToken撤回消息
     * @param msg
     * @return
     */
    @PostMapping("/revoke/multi/pushToken")
    public DeferredResult<UPushResponse> revokeFromTokens(@Validated @RequestBody URevokeMessageTokenMulti msg){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokensRevoke(msg.getMessage(), msg.getPushTokens())
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 给指定用户推送消息
     * @param msg
     * @param packageName 可选，给userId绑定的包名为packageName推送，多个包名时用;分割。下面接口不再赘述
     * @param fromToken   可选，消息同步推送给发送者其他设备时排除发送设备
     * @return
     */
    @PostMapping("/user")
    public DeferredResult<UPushResponse> pushToUser(@Validated @RequestBody UMessageUser msg,
                                                    @RequestParam(value = "packageName", required = false) String packageName,
                                                    @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.userPush(msg.getMessage(), msg.getUserId())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 给多个用户推送消息
     * @param msg
     * @param packageName
     * @param fromToken
     * @return
     */
    @PostMapping("/multi/user")
    public DeferredResult<UPushResponse> pushToUsers(@Validated @RequestBody UMessageUserMulti msg,
                                                     @RequestParam(value = "packageName", required = false) String packageName,
                                                     @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.usersPush(msg.getMessage(), msg.getUserIds())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 从指定用户撤回消息
     * @param msg
     * @param packageName
     * @param fromToken   可选，消息同步从发送者其他设备撤回时排除发送设备
     * @return
     */
    @PostMapping("/revoke/user")
    public DeferredResult<UPushResponse> revokeFromUser(@Validated @RequestBody URevokeMessageUser msg,
                                                        @RequestParam(value = "packageName", required = false) String packageName,
                                                        @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.userRevoke(msg.getMessage(), msg.getUserId())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 从多个用户撤回消息
     * @param msg
     * @param packageName
     * @param fromToken
     * @return
     */
    @PostMapping("/revoke/multi/user")
    public DeferredResult<UPushResponse> revokeFromUsers(@Validated @RequestBody URevokeMessageUserMulti msg,
                                                         @RequestParam(value = "packageName", required = false) String packageName,
                                                         @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.usersRevoke(msg.getMessage(), msg.getUserIds())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(response));
        return result;
    }


    /**
     * 发送群组消息
     * @param packageName 可选，给客户端包名为packageName推送，多个包名时用;分割。下面接口不再赘述
     * @param fromToken   可选，推送群消息时排除掉发送设备
     * @return
     */
    @PostMapping("/group")
    public DeferredResult<UPushResponse> groupPush(@Validated @RequestBody UMessageGroupDto dto,
                                                   @RequestParam(value = "packageName", required = false) String packageName,
                                                   @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.groupPush(dto.getMessage(), dto.getGroupId())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(appendMsgId(response, dto.getMessage().getMsgId())));
        return result;
    }

    /**
     * 群组消息撤回
     * @param dto
     * @param packageName
     * @param fromToken   可选，群消息撤回时排除掉发起撤回的设备
     * @return
     */
    @PostMapping("/revoke/group")
    public DeferredResult<UPushResponse> groupRevoke(@Validated @RequestBody UGroupRevokeMessageDto dto,
                                                     @RequestParam(value = "packageName", required = false) String packageName,
                                                     @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.groupRevoke(dto.getMessage(), dto.getGroupId())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 给全体推送消息
     * @param msg
     * @param packageName 可选，给客户端包名为packageName推送，多个包名时用;分割
     * @param fromToken   可选，全体推送消息时排除掉发送设备
     * @return
     */
    @PostMapping("/all")
    public DeferredResult<UPushResponse> pushAll(@Validated @RequestBody UMessageAllDto msg,
                                                 @RequestParam(value = "packageName", required = false) String packageName,
                                                 @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.pushAll(msg.getMessage())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 向全体广播消息，只有在线的能收到
     * @param msg
     * @param packageName 可选，给客户端包名为packageName推送，多个包名时用;分割
     * @param fromToken   可选，全体推送消息时排除掉发送设备
     * @return
     */
    @PostMapping("/broadcast")
    public DeferredResult<UPushResponse> broadcast(@Validated @RequestBody UMessageAllDto msg,
                                                   @RequestParam(value = "packageName", required = false) String packageName,
                                                   @RequestParam(value = "fromToken", required = false) String fromToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.broadcast(msg.getMessage())
                .filterPackageName(packageName)
                .excludeFromToken(fromToken)
                .send(response -> result.setResult(appendMsgId(response, msg.getMessage().getMsgId())));
        return result;
    }

    /**
     * 延迟消息撤回
     * @param dto
     * @return
     */
    @PostMapping("/revoke/delay")
    public DeferredResult<UPushResponse> delayMessageRevoke(@Validated @RequestBody DelayMessageRevokeDto dto) {
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.delayMessageRevoke(dto.getMsgId())
                .send(response -> result.setResult(response));
        return result;
    }

    private UPushResponse appendMsgId(UPushResponse response, String msgId) {
        if(response.getCode() <= ErrorCode.SUCCEED_SOME){
            if(response.getData() == null){
                Map<String, String> map = new HashMap<>(1);
                map.put("msgId", msgId);
                response.setData(map);
            }else{
                if(response.getData() instanceof Map){
                    Map map = (Map)response.getData();
                    map.put("msgId", msgId);
                }
            }
        }

        return response;
    }
}
