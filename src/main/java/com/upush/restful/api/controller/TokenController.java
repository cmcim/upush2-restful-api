/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/16
 * @Description:
 */

package com.upush.restful.api.controller;

import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushClient;
import com.upush.api.UPushResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/pushToken")
public class TokenController {

    @Autowired
    private UPushClient pushClient;


    /**
     * 业务服务器发现pushToken变更后调用此方法
     * @param previousToken
     * @param currentToken
     * @return
     */
    @PostMapping("/{previousToken}/to/{currentToken}")
    public DeferredResult<UPushResponse> tokenReplace(@PathVariable("previousToken") String previousToken,
                                      @PathVariable("currentToken") String currentToken){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.tokenReplace(previousToken, currentToken)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 设置pushToken的通知类型
     * @param pushToken
     * @param notifyType
     * @return
     */
    @PostMapping("/{pushToken}/notification/type/{notifyType}")
    public DeferredResult<UPushResponse> setNotifyTypeForToken(@PathVariable("pushToken") String pushToken,
                                                               @PathVariable("notifyType") Integer notifyType){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setNotifyTypeForToken(pushToken, notifyType)
                .send(response -> result.setResult(response));
        return result;
    }
}
