/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.controller;


import com.upush.restful.api.dto.PortraitDto;
import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushClient;
import com.upush.api.UPushResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UPushClient pushClient;


    /**
     * 设置userId接收来自fromId消息的通知类型
     * @param userId
     * @param fromId
     * @param notifyType
     * @return
     */
    @PostMapping("/{userId}/from/{fromId}/notification/type/{notifyType}")
    public DeferredResult<UPushResponse> setNotifyTypeForFromId(@PathVariable("userId") String userId,
                                                                @PathVariable("fromId") String fromId,
                                                                @PathVariable("notifyType") Integer notifyType){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setNotifyTypeForFromId(fromId, userId, notifyType)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * fromId发送消息给userId时，显示fromId的别名
     * @param userId
     * @param fromId
     * @param alias
     * @return
     */
    @PostMapping("/{userId}/from/{fromId}/alias/{alias}")
    public DeferredResult<UPushResponse> setAliasForFromId(@PathVariable("userId") String userId,
                                                           @PathVariable("fromId") String fromId,
                                                           @PathVariable("alias") String alias){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setAliasForFromId(fromId, userId, alias)
                .send(response -> result.setResult(response));
        return result;
    }

    /**
     * 设置userId的用户画像
     * @param portrait
     * @return
     */
    @PostMapping("/portrait")
    public DeferredResult<UPushResponse> setPortraitForFromId(@Validated @RequestBody PortraitDto portrait){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.setPortraitForFromId(portrait.getUserId(), portrait.getName(), portrait.getAvatar())
                .send(response -> result.setResult(response));
        return result;
    }
}
