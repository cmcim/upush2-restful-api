/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.controller;

import com.upush.api.assign.AssignedClientInfo;
import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushClient;
import com.upush.api.UPushResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("/")
public class OtherController {

    @Autowired
    private UPushClient pushClient;

    /**
     * 取得一个connector地址，用于从业务服务器分配一个connector给客户端
     * @param clientInfo
     * @return
     */
    @GetMapping("assign/connector/to/client")
    public DeferredResult<UPushResponse> assignConnectorServer(@RequestBody AssignedClientInfo clientInfo){
        DeferredResult<UPushResponse> result = SpringContextUtil.createDeferredResult();
        pushClient.assignConnectorServer(clientInfo)
                .send(response -> result.setResult(response));
        return result;
    }
}
