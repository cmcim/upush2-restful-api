/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.interceptor;

import com.upush.api.UPush2ApiException;
import com.upush.api.UPushResponse;
import com.upush.api.constant.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@Slf4j
@ControllerAdvice
public class ExceptionInterceptor {

    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidException(Exception e) throws Exception {
        e.printStackTrace();
        UPushResponse response = new UPushResponse();
        response.setCode(ErrorCode.INVALID_PARAMS);
        response.setMsg(e.getMessage());
        return response;
    }

    @ResponseBody
    @ExceptionHandler(value = UPush2ApiException.class)
    public Object upushApiException(UPush2ApiException e) {
        UPushResponse response = new UPushResponse();
        response.setCode(e.getCode());
        response.setMsg(e.getMessage());
        return response;
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Object commonExceptionHandler(Exception e){
        e.printStackTrace();

        UPushResponse response = new UPushResponse();
        response.setCode(ErrorCode.SYSTEM_ERROR);
        response.setMsg(e.getMessage());

        return response;
    }

}
