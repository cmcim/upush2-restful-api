/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.interceptor;

import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPush2ApiException;
import com.upush.api.constant.ErrorCode;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String auth = httpServletRequest.getHeader("Authorization");
        String secret = null;
        if(auth != null){
            String[] arr = auth.split("=");
            if(arr.length > 1 && "key".equals(arr[0])){
                secret = arr[1];
                SpringContextUtil.setRequestContext("secret", secret);
            }
        }

        if(secret == null || secret.isEmpty()){
            throw new UPush2ApiException(ErrorCode.NO_AUTH_CONN, "Not find auth info");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
    }
}
