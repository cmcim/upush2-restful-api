/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.interceptor;

import com.upush.restful.api.util.SpringContextUtil;
import com.upush.api.UPushResponse;
import com.upush.api.constant.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Slf4j
@Aspect
@Component
public class LogInterceptor {

    @Pointcut("execution(* com.upush.restful.api.controller..*.*(..))" +
            " && (@annotation(org.springframework.web.bind.annotation.GetMapping)" +
            " || @annotation(org.springframework.web.bind.annotation.PostMapping)" +
            " || @annotation(org.springframework.web.bind.annotation.PutMapping)" +
            " || @annotation(org.springframework.web.bind.annotation.DeleteMapping)" +
            " || @annotation(org.springframework.web.bind.annotation.RequestMapping))")
    public void valid() {}


    @Around("valid()")
    public Object arround(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request = SpringContextUtil.getRequest();
        Object[] objects = pjp.getArgs();

        if(request != null){
            logReq(request, objects);
        }

        Object ret = null;
        try {
            ret = pjp.proceed();
        } catch (Throwable throwable) {
            throw throwable;
        }
        logRsp(request, ret);
        return ret;
    }

    private void logReq(HttpServletRequest request, Object[] objects) {
        if(objects.length == 0) {
            log.info("{} [{}] {}",  SpringContextUtil.getIpAddr(), request.getMethod(), request.getServletPath());
        } else {
            StringBuilder sb = new StringBuilder();
            for(Object ob : objects){
                if(ob == null || ob instanceof HttpServletRequest
                        || ob instanceof HttpServletResponse
                        || ob instanceof Principal){
                    continue;
                }

                // 数组参数判断
                if(ob.getClass().isArray()){
                    for(Object o: (Object[]) ob){
                        if(o instanceof MultipartFile){
                            sb.append(((MultipartFile) o).getOriginalFilename()).append(", ");
                        }else{
                            sb.append(o.toString()).append(", ");
                        }
                    }
                }else{
                    if(ob instanceof MultipartFile){
                        sb.append(((MultipartFile) ob).getOriginalFilename()).append(", ");
                    }else{
                        sb.append(ob.toString()).append(", ");
                    }
                }
            }
            if(sb.length() > 2){
                sb.deleteCharAt(sb.length() - 2);
            }
            String params = sb.toString();

            log.info("{} [{}] {} => {}", SpringContextUtil.getIpAddr(), request.getMethod(), request.getServletPath(), params);
        }
    }

    private void logRsp(HttpServletRequest request, Object ret){
        if(request == null){
            return;
        }

        if(ret instanceof UPushResponse){
            if(((UPushResponse) ret).getCode() == ErrorCode.SUCCEED){
                log.info("[Response] {} => {}", request.getServletPath(), ret.toString());
            }else{
                log.error("[Response] {} => {}",  request.getServletPath(), ret.toString());
            }
        }
    }
}
