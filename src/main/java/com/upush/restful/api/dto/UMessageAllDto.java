/**
 * @Author: lzk@u-call.net
 * @Date: 2022/5/22
 * @Description:
 */

package com.upush.restful.api.dto;

import com.upush.api.model.UMessageGroup;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
public class UMessageAllDto {
    @NotNull
    private UMessageGroup message;
}
