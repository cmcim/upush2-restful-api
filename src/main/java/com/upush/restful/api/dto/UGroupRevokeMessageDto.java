/**
 * @Author: lzk@u-call.net
 * @Date: 2022/5/22
 * @Description:
 */

package com.upush.restful.api.dto;

import com.upush.api.model.URevokeMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
public class UGroupRevokeMessageDto {
    @NotNull
    private URevokeMessage message;
    @NotBlank
    private String groupId;
}
