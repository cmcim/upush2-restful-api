/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.dto;

import com.upush.api.model.URevokeMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@ToString
public class URevokeMessageUserMulti {
    @NotNull
    private URevokeMessage message;
    @NotEmpty
    private List<String> userIds;
}
