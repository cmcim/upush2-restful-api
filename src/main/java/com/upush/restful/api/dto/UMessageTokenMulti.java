/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.dto;

import com.upush.api.model.UMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@ToString
public class UMessageTokenMulti {
    @NotNull
    private UMessage message;
    @NotEmpty
    private List<String> pushTokens;
}
