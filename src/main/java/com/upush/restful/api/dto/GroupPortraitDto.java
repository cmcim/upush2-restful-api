/**
 * @Author: lzk@u-call.net
 * @Date: 2022/5/22
 * @Description:
 */

package com.upush.restful.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
public class GroupPortraitDto {
    @NotBlank
    private String groupId;
    private String name;
    private String avatar;
}
