/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/18
 * @Description:
 */

package com.upush.restful.api.util;

import com.upush.api.UPushResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;

public class SpringContextUtil {

    public static HttpServletRequest getRequest(){
        ServletRequestAttributes sr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if(sr != null){
            return sr.getRequest();
        }

        return null;
    }

    public static String getIpAddr() {
        HttpServletRequest request = SpringContextUtil.getRequest();
        String ip = request.getHeader("X-Real-IP");
        if (ip != null && !ip.isEmpty() && !"unknown".equalsIgnoreCase(ip)) {
            return ip;
        }
        ip = request.getHeader("X-Forwarded-For");
        if (ip != null && !ip.isEmpty() && !"unknown".equalsIgnoreCase(ip)) {
            int index = ip.indexOf(',');
            if (index != -1) {
                return ip.substring(0, index);
            } else {
                return ip;
            }
        } else {
            return request.getRemoteAddr();
        }
    }

    public static void setRequestContext(String key, String value){
        HttpServletRequest request = SpringContextUtil.getRequest();
        if(request != null){
            request.setAttribute(key, value);
        }
    }

    public static String getRequestContext(String key){
        HttpServletRequest request = SpringContextUtil.getRequest();
        if(request != null){
            return (String) request.getAttribute(key);
        }

        return null;
    }

    public static String getRequestSecret() {
        return getRequestContext("secret");
    }

    public static DeferredResult<UPushResponse> createDeferredResult() {
        return new DeferredResult<>(30000L);
    }
}
