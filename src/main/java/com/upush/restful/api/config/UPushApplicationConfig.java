/**
 * @Author: lzk@u-call.net
 * @Date: 2022/1/16
 * @Description:
 */

package com.upush.restful.api.config;

import com.upush.restful.api.interceptor.AuthenticationInterceptor;
import com.upush.api.UPushClient;
import com.upush.api.UPushFailedListener;
import com.upush.api.model.UMessage;
import com.upush.api.model.URevokeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
@EnableWebMvc
public class UPushApplicationConfig implements WebMvcConfigurer  {

    @Value("${upush.host}")
    private String upushHost;

    @Value("${upush.port}")
    private Integer upushPort;

    @Value("${upush.app-secret}")
    private String appSecret;

    @Bean
    public FilterRegistrationBean corsFilter() {
        final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        // 跨域访问配置
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(true);//是否支持证书
        corsConfiguration.addAllowedOrigin("*");//是否允许添加Origin
        corsConfiguration.addAllowedHeader("*");//是否允许请求头
        corsConfiguration.addAllowedMethod("*");//是否允许添加http方法
        corsConfiguration.setMaxAge(3600L);
        //跨域访问配置的路径
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        //return new CorsFilter(urlBasedCorsConfigurationSource);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(urlBasedCorsConfigurationSource));
        bean.setOrder(0);//配置CorsFilter优先级
        return bean;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthenticationInterceptor())
                .order(11)
                .addPathPatterns("/**");
    }

    @Bean
    public UPushClient pushClient(){
        UPushClient pushClient = null;
        if(appSecret == null || appSecret.isEmpty()){
            pushClient = new UPushClient(upushHost, upushPort);
        }else{
            pushClient = new UPushClient(appSecret, upushHost, upushPort);
        }
        return pushClient;
    }

    /*@Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setMaxPayloadLength(64000);
        return loggingFilter;
    }*/
}
