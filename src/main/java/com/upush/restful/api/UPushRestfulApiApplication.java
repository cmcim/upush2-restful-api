package com.upush.restful.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UPushRestfulApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UPushRestfulApiApplication.class, args);
	}

}
